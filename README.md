# README #

To run code, simply double click the solution (.sln) file. Assuming you have the proper version of Visual Studio, this will launch the IDE. Within the IDE, press F7 or ctrl+shift+b to compile, then press F5 to run.

### What is this repository for? ###

This repository is meant to host LAPACK and BLAS libraries that can be used by Visual Studio. Currently, 64-bit libraries for Visual Studio 2013 (version 12), Visual Studio 2015 (version 14), Visual Studio 2019 (version 14.2), and Visual Studio 2022 (version 14.3) are available. The LAPACK versions are 3.5.0 for VS2013, 3.6.1 for VS2015, 3.9.0 for VS2019, 3.11.0 for VS2022.

Note, my new machine only has VS2022, hence why older VS libraries were not recompiled with latest source and tools.

### How were the libraries built? ###

Using the instructions located at [LAPACK-FOR-WINDOWS](http://icl.cs.utk.edu/lapack-for-windows/lapack/index.html#build)

#### Building LAPACK 64-bit libraries for Visual Studio 2013, 2015, 2019, 2022 ####

The following are build details for the 64-bit LAPACK libraries for VS2013 with inline comments for more recent versions of Visual Studio such as VS2019 and VS2022. They are only relevant if you want to build your own LAPACK libraries instead of using the ones in this repository.

For VS2015 libraries, similar procedure was used with LAPACK source 3.6.1, CMake 3.6, and MinGW-64 6.1.0.

For VS2019 libraries, similar procedure as before but with LAPACK source 3.9.0, Visual Studio 2019 Community (14.2), built using CMake 3.15.3 and MinGW-64 8.1.0

For VS2022 libraries, similar procedure but with LAPACK source 3.11.0, Visual Studio 2022 Community (14.3), built using CMake 3.26.4 and winlibs version of MinGW-w64 11.0.0 linked with UCRT (specific build = gcc-13.1.0-llvm-16.0.2-mingw-w64ucrt-11.0.0-r1)

For these instructions, let's assume the following paths (most of these are arbitrary and you can change them for your build). Note, I will refer to these paths later on by their preceding keyword (e.g. - _SourceDir_)

* _SourceDir_: The LAPACK source directory (where lapack.tgz was extracted to) is `C:\dev\lapack-3.5.0`
* _BuildDir_: The target build folder to be used by CMake is `C:\dev\build`
* _GccPath_: The path to gcc.exe is `C:\dev\mingw64\bin\gcc.exe`
* _GfortranPath_: The path to gfortran.exe is `C:\dev\mingw64\bin\gfortran.exe`
* _MakePath_: The path to mingw32-make.exe is `C:\dev\mingw64\bin\mingw32-make.exe`
* _VcVars64Path_: The path for setting up the visual studio build environment is `C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin\amd64\vcvars64.bat`
  * **Update:** For Visual Studio Community 2019, the default path is now `C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat` 
  * **Update:** For Visual Studio Community 2022, the default path is now `C:/Program Files/Microsoft Visual Studio/2022/Community/VC/Auxiliary/Build/vcvars64.bat`
Okay, now that we've defined the relevant paths, let's get LAPACK building. 

1. Download and extract LAPACK source: [lapack.tgz](http://www.netlib.org/lapack/index.html)
2. Download and install [CMake](http://www.cmake.org/)
3. Download and install the [MinGW 64 bit compiler](http://mingw-w64.sourceforge.net/)
  - Version used for VS2022 buid: 11.0.0 linked with UCRT [winlibs project](https://winlibs.com/) [git](https://github.com/brechtsanders/winlibs_mingw/releases/download/13.1.0-16.0.2-11.0.0-ucrt-r1/winlibs-x86_64-mcf-seh-gcc-13.1.0-llvm-16.0.2-mingw-w64ucrt-11.0.0-r1.7z)
  - Version used for VS2013 build: 4.9.2 of [MinGW builds project](http://sourceforge.net/projects/mingw-w64/) (latest at time) (Note: in the install dialog, select x86_64, Win32 threads, and SEH exceptions
1. Run the CMake GUI
  - Set 'where is the source code' to _SourceDir_  
  - Set 'where to build binaries' to _BuildDir_
1. Press 'Configure' button  
  - Set generator to 'MinGW Makefiles'
1. Select 'specify native compilers' and set:  
  - C compiler to _GccPath_  
  - Fortran compiler to _GfortranPath_
1. Press 'Finished' button

CMake will now attempt to configure the build. You will likely get a lot of configuration errors listed in red. The basic idea for the next part is to address the red "error" fields by either ignoring them or setting them to the proper value. Pressing the Configure button again should turn these fields white and then generate even more red fields. Repeat the process (address the red fields as necessary then press the Configure button) until eventually all fields are white.

Here are some red-colored CMake configuration errors that I specifically addressed (everything else was ignored - i.e. - no change was made)

* Make sure CMAKE_MAKE_PROGRAM is set to _MakePath_
* Make sure CMAKE_SH is set to `CMAKE_SH-NOTFOUND` (ignore this error if this is already the case)
* Make sure CMAKE_USE_RELATIVE_PATHS is unchecked (ignore this error if this is already the case)
  * Note, this error did not appear in later CMake version 3.15.3
* Make sure BUILD_SHARED_LIBS is checked (creates a .dll)
* Make sure CMAKE_GNUtoMS is checked (this is what allows gcc libs to be used in visual studio)
* Make sure VCVARSAMD64 is set to _VcVars64Path_
  * Note, this error did not appear in later CMake version 3.15.3
* Make sure CMAKE_GNUtoMS_VCVARS is set to _VcVars64Path_
* For LAPACK 3.9.0, a downstream linker error was encountered while building (see following [discussion](http://icl.cs.utk.edu/lapack-forum/viewtopic.php?f=4&t=5315) dealing with  multiple definitions of *_gfortran_concat_string*) which can be avoided by setting CMAKE_Fortran_FLAGS to `-Wl,--allow-multiple-definition`
* Optional: If you want to build the LAPACKE library, set the LAPACKE option to ON

1. Once all configuration fields turn white, press the Generate button.  
2. Close CMake, open up a command line (`cmd.exe`, have *not* tested with powershell), and navigate to the _BuildDir_  
3. Within the _BuildDir_, type "_MakePath_" (no quotes). **Note**, for VS2022 build, I initially received an unable to find `libgfortran-5.dll` error; to resolve I had to add the MinGW _bin_ folder (i.e. the parent folder containing gcc, gfortran, and ming32-make, e.g. `C:\dev\mingw64\bin` in our assumed file structure) to be the first entry in the System `Path` [environment variable](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_environment_variables?view=powershell-7.3#saving-environment-variables-with-the-system-control-panel), I was then able to simply type `ming32-make` and this allowed a successful compile.

4. That's it, libblas and liblapack .dlls will be located in the _BuildDir_\bin folder and the visual studio wrapper .libs will be in the _BuildDir_\lib folder
5. Optional: to run included tests, type "_MakePath_ test" from within _BuildDir_